all: clean Start server db

Start:
	makensis Start.nsi

server: server.zip
	cat lib/unzip/unzipsfx-gcc.exe lib/server.zip > app/server.exe
	zip -A app/server.exe

server.zip:
	cd lib/server && zip -r ../server.zip *

db: db.zip
	cat lib/unzip/unzipsfx-gcc.exe db.zip > app/db.exe
	zip -A app/db.exe

db.zip:
	cd db && zip -r ../db.zip *

clean:
	rm -rf app/Start.exe
	rm -rf app/server.exe
	rm -rf server.zip
	rm -rf app/db.exe
	rm -rf db.zip
