Name 'My Drupal Site'
OutFile 'app\Start.exe'
RequestExecutionLevel highest

Function .onInit
  SetSilent silent
FunctionEnd

Section
  Banner::show "Starting, please wait ..."
  !define InsDir '$WINDIR\Temp\webdemo'
  !define DbDir  '${InsDir}\usr\local\mysql\data\webdemo'
  !define StopCmd  

  ; Create Directory in Host
  CreateDirectory '${InsDir}'

  ; Copy Server and Database to TEMP directory
  nsExec::exec '"$EXEDIR\server.exe" -q -o -d "${InsDir}"'
  Banner::destroy
  Banner::show "Completed 40%, please wait ..."
  nsExec::exec '"$EXEDIR\db.exe" -q -o -d "${DbDir}"'
  Banner::destroy
  Banner::show "Completed 60%, please wait ..."

  ;~ ; Start Web Server
  Banner::destroy
  Banner::show "Starting web browser, please wait ..."
  ; Debug: ExecWait '"${InsDir}\usr\local\php\php.exe" -n "${InsDir}\unicon\main\start_servers.php" 7 "$EXEDIR"'
  nsExec::exec '"${InsDir}\usr\local\php\php.exe" -n "${InsDir}\unicon\main\start_servers.php" 7 "$EXEDIR"'

  Banner::destroy
  MessageBox MB_OK|MB_ICONINFORMATION 'You have just started the CD of My Drupal Site.$\r$\n$\r$\nWARNING !$\r$\nDO NOT press button "OK" till you have navigated.'

  ; Stop Web Server
  Banner::destroy
  Banner::show "Closing, please wait ..."
  nsExec::exec '"${InsDir}\usr\local\php\php.exe" -n "${InsDir}\unicon\main\stop_servers.php"'

  ; Remove Server and Database
  Sleep 2000
  RMDir /r '${InsDir}'
  Sleep 1000
SectionEnd
